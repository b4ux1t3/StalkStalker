﻿using System.Text.Json;
using Blazored.LocalStorage;
using StalkStalker.Data;
using StalkStalker.Extensions;

namespace StalkStalker.Services;

public class Tracker
{
    public List<Week> Weeks { get; private set; }
    public Week? CurrentWeek => Weeks.FirstOrDefault(week => week.StartDate == DateTime.Now.GetSunday());
    private readonly ILocalStorageService? _localStorageAsync;

    public Tracker(ILocalStorageService localStorageAsync, ISyncLocalStorageService localStorage)
    {
        var _localStorage = localStorage;
        _localStorageAsync = localStorageAsync;

        List<Week> weeks;
        try
        {
            weeks = _localStorage.GetItem<List<Week>>("history");
        }
        catch (JsonException)
        {
            weeks = new();
        }

        Weeks = weeks?.Count > 0 ? weeks : new();
    }

    public void NewWeek(uint turnips, uint price)
    {
        var sunday = DateTime.Now.GetSunday();
        var newWeek = new Week(sunday, turnips, price); 
        Weeks.Add(newWeek);
    }
    
    public async Task NewWeek(Week week)
    {
        Weeks.Add(week);
        await WriteToStorage();
    }

    public async Task WriteToHistory()
    {
        await WriteToStorage();
    }

    public async Task<bool> AddSale(Sale sale)
    {
        if (CurrentWeek == null) return false;
        var success = CurrentWeek.TryAddSale(sale);
        if (success) await WriteToHistory();

        return success;
    }

    public async Task<bool> AddSale(Sale sale, Guid? id)
    {
        if (id == null) return await AddSale(sale);
        var targetWeek = Weeks.First(week => week.Id == id);
        var success = targetWeek.TryAddSale(sale);
        if (success) await WriteToHistory();
        return success;
    }

    public async Task DeleteSale(Sale sale)
    {
        CurrentWeek?.DeleteSale(sale);
        await WriteToStorage();
    }
    public async Task DeleteSale(Sale sale, Guid? id)
    {
        if (id == null)
        {
            await DeleteSale(sale);
            return;
        }
        
        var targetWeek = Weeks.First(week => week.Id == id);
        targetWeek.DeleteSale(sale);
        await WriteToStorage();
    }
    private async Task WriteToStorage()
    {
        if (_localStorageAsync != null) await _localStorageAsync.SetItemAsync("history", Weeks);
    }

    public Week? FindWeek(Guid id) => Weeks.FirstOrDefault(week => week.Id == id);

    public async Task UpdateWeekStats(Guid weekId, uint price, uint turnips)
    {
        var week = FindWeek(weekId);
        week?.UpdateStats(price, turnips);
        await WriteToStorage();
    }
}