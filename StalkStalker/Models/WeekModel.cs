﻿using System.ComponentModel.DataAnnotations;
using StalkStalker.Data;
using StalkStalker.Extensions;

namespace StalkStalker.Models;

public class WeekModel
{
    [Required] public int Turnips;
    [Required] public int Price;

    public Week CastToWeek()
    {
        return new Week(DateTime.Now.GetSunday(), (uint) Turnips, (uint) Price);
    }
}