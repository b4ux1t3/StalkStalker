using StalkStalker.Data;

namespace StalkStalker.Models;

public class DayModel
{
    public static readonly string[] DaysOfWeek = new[] {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    public int MorningPrice { get; set; }
    public int MiddayPrice { get; set; }
}