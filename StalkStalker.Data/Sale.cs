using System.Text.Json.Serialization;

namespace StalkStalker.Data;

public record Sale(uint Turnips, uint Price, DateTime Date)
{
    [JsonIgnore]
    public uint Income => Turnips * Price;
}