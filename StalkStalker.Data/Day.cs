﻿using System.Text.Json.Serialization;

namespace StalkStalker.Data;

[method: JsonConstructor]
public class Day(uint morningPrice, uint middayPrice, Guid id, DateTime date)
{
    public static readonly string[] DaysOfWeek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    private uint _morningPrice = morningPrice;

    public Guid Id { get; private set; } = id;

    public uint MorningPrice
    {
        get => _morningPrice;
        private set => _morningPrice = value;
    }

    public uint MiddayPrice { get; private set; } = middayPrice;
    public DateTime Date { get; private set; } = date;

    [JsonIgnore]
    public float Average => (MiddayPrice + MorningPrice) / 2f;

    public Day(DateTime date) : this(0, 0, Guid.NewGuid(), date)
    {
    }

    public void SetMorning(uint price)
    {
        MorningPrice = price;
    }
    public void SetMidday(uint price)
    {
        MiddayPrice = price;
    }

    public override string ToString()
    {
        return $"Morn: {MorningPrice} | Mid: {MiddayPrice}";
    }
}