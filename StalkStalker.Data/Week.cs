﻿using System.Text;
using System.Text.Json.Serialization;

namespace StalkStalker.Data;

public class Week
{
    public Guid Id { get; }
    public DateTime StartDate { get; }
    public uint Turnips { get; private set; }
    public uint Price { get; private set; }
    public long Spent => Turnips * Price;
    public Day[] Days { get; }
    public List<Sale> Sales { get; }
    [JsonIgnore]
    public long Revenue => Sales.Count > 0 ? Sales.Select(sale => sale.Income).Aggregate((x, y) => x + y) : 0;
    [JsonIgnore]
    public long Profit => Revenue - Spent;
    [JsonIgnore]
    public uint TurnipsSold => Sales.Count > 0 ? Sales.Select(sale => sale.Turnips).Aggregate((x, y) => x + y) : 0;

    [JsonIgnore] public double ProfitPercentage => Spent > 0 ? (((double)Profit / Spent) * 100) : 100;
    public Week(DateTime startDate, uint turnips, uint price)
    {
        Id = Guid.NewGuid();
        StartDate = startDate;
        Turnips = turnips;
        Price = price;
        Days =
        [
            new(StartDate.AddDays(1)), 
            new(StartDate.AddDays(2)), 
            new(StartDate.AddDays(3)), 
            new(StartDate.AddDays(4)), 
            new(StartDate.AddDays(5)), 
            new(StartDate.AddDays(6))
        ];
        Sales = [];

    }
    
    [JsonConstructor]
    public Week(Guid id, DateTime startDate, uint turnips, uint price, Day[] days, List<Sale> sales)
    {
        Id = id;
        StartDate = startDate;
        Turnips = turnips;
        Price = price;
        Days = days;
        Sales = sales;
    }

    public bool TryAddSale(Sale sale)
    {
        if (Sales.Count == 0)
        {
            Sales.Add(sale);
            return true;
        }
        var remainingTurnips = Turnips - Sales.Select(_sale => _sale.Turnips).Aggregate((x, y) => x + y);
        if (sale.Turnips > remainingTurnips) return false;
        Sales.Add(sale);
        return true;
    }
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append($"{StartDate.ToShortDateString()}\nT:{Turnips}\nP:{Price}\nS:{Spent}\n");
        for (var i = 0; i < Days.Length; i++)
        {
            sb.Append($"{Day.DaysOfWeek[i]}: {Days[i]}\n");
        }
        return sb.ToString();
    }

    public void DeleteSale(Sale sale)
    {
        Sales.Remove(sale);
    }

    public void UpdateStats(uint price, uint turnips)
    {
        Price = price;
        Turnips = turnips;
    }
}