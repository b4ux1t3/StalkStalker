﻿namespace StalkStalker.Extensions;

public static class DateTimeExtensions
{
    public static DateTime GetSunday(this DateTime date)
    {
        var sunday = date.Subtract(TimeSpan.FromDays((int) DateTime.Now.DayOfWeek));
        return new DateTime(sunday.Year, sunday.Month, sunday.Day);
    }
}