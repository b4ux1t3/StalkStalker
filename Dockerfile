FROM mcr.microsoft.com/dotnet/sdk:6.0 as build
WORKDIR /build
COPY . .
RUN dotnet publish -c Release -o out

FROM nginx:latest as run
COPY --from=build /build/out/wwwroot /usr/share/nginx/html
COPY ./default.conf  /etc/nginx/conf.d/

