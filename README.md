# Stalk Stalker

[Hosted right here on GitLab](https://b4ux1t3.gitlab.io/StalkStalker)

This is a simple tool that will allow you to keep track of profits (and missed profits) for you Turnip Stonks in Animal Crossing: New Horizons.

It should be noted that this is not a competitor to [Turnip Prophet](https://turnipprophet.io). This application will not predict trends for you.

Rather, this application keeps track of your actual, fungible profits. You enter how many turnips you purchased and at what price, and how many turnips you _sell_ at a given price, and you will get the amount you gain (or lose) in cold, hard Bells.

This has some advantages when used in conjunction with Turnip Prophet (or any other such app). For example, Turnip Prophet et al. only take _your_ island's turnip prices into the equation. If you buy turnips on another player's island, entering that price into Turnip Prophet can throw off its calculations. You can keep track of your "actuals" in this app.

## A Note on Privacy

This tool runs exclusively in your browser. Once you access the web page above, no further web connections will leave your machine from this app. As such, I do not (and _can not_) store any of your data. This also means that all of your data is yours to do with what you will. If you want to delete your data, you just have to "Clear Site Data" in your chosen browser.

I intend to write "save to local machine" and "load from local machine" functionality into the app (see #7). Until that's in place, your data only lives in your browser's Local Storage area.
